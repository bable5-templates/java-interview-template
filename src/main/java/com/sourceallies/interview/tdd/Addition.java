package com.sourceallies.interview.tdd;

@FunctionalInterface
public interface Addition<A> {

    A add(A a1, A a2);
}

package com.sourceallies.interview.tdd;



import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class Demo1Test {

    @Mock
    private Addition<String> mockAddr;

    @InjectMocks
    private Demo1 demo1;

    @BeforeEach
    public void setup() {
        when(mockAddr.add(any(), any())).thenReturn("WOLDDDD");
    }

    @Test
    public void test1() {
        String s = demo1.concatStrings("1", "2");

        assertThat(s, is("WOLDDDD"));
        verify(mockAddr).add("1", "2");
    }
}

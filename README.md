# Source Allies Java Interviews

Java interview template. 

### TDD

Includes junit, hamcrest and mockito.

### Getting Started

```sh
$> ./gradlew test
```
